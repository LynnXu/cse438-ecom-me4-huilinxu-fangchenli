package com.example.cse438.studio4

import android.app.ActionBar
import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.view.Gravity
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import com.example.cse438.studio4.model.Review
import com.example.cse438.studio4.viewmodel.ProductViewModel
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.product_review_list_item.view.*

class App {
    companion object {
        var firebaseAuth: FirebaseAuth? = null

        fun openReviewDialog(context: Context, itemId: String) {
            val dialog = Dialog(context)

            dialog.setContentView(R.layout.dialog_add_review)

            val window = dialog.window
            window?.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)

            dialog.findViewById<Button>(R.id.close).setOnClickListener {
                dialog.dismiss()
            }

            dialog.findViewById<Button>(R.id.submit).setOnClickListener {
                val body = dialog.findViewById<TextView>(R.id.body).text.toString()
                val isAnonymous = dialog.findViewById<CheckBox>(R.id.anonymous).isSelected
                val userId = App.firebaseAuth?.currentUser?.uid

                if (userId != null && body != "") {
                    val db = FirebaseFirestore.getInstance()

                    db.document("items/$itemId").get().addOnCompleteListener { it1 ->
                        if (it1.isSuccessful) {

                            var itemList = it1.result?.get("reviews") as? ArrayList<Review>

                            if (itemList == null) {
                                itemList = arrayListOf()
                            }

                            db.document("users/$userId").get().addOnCompleteListener{ it2 ->
                                if (it2.isSuccessful) {
                                    val userData = it2.result

                                    var reviewList = userData?.get("reviews") as? ArrayList<Review>

                                    if (reviewList!!.size == 0) reviewList = arrayListOf()

                                    val itemWithId = itemList[itemId.toInt()]
                                    if (!reviewList.contains(itemWithId)) {
                                        reviewList.add(itemWithId)
                                    }

                                    val newReview = Review(
                                        body, isAnonymous, userId,
                                        userData?.get("username") as String, Timestamp.now()
                                    )

                                    reviewList.add(newReview)

                                    val itemMap = hashMapOf(
                                        "reviews" to reviewList.toList()
                                    )

                                    db.document("items/$itemId").set(itemMap)

                                    val userMap = hashMapOf(
                                        "first_name" to userData.get("first_name"),
                                        "last_name" to userData.get("first_name"),
                                        "email" to userData.get("email"),
                                        "username" to userData.get("username"),
                                        "reviews" to reviewList.toList()
                                    )

                                    db.document("users/$userId").set(userMap)
                                }
                            }
                        }
                    }
                    val toast = Toast.makeText(context, "Review Submitted!.", Toast.LENGTH_SHORT)
                    toast.setGravity(Gravity.CENTER, 0, 0)
                    toast.show()
                    dialog.dismiss()
                } else {
                    val toast = Toast.makeText(context, "Unable to submit review!.", Toast.LENGTH_SHORT)
                    toast.setGravity(Gravity.CENTER, 0, 0)
                    toast.show()
                }
            }
            dialog.show()
        }
    }
}