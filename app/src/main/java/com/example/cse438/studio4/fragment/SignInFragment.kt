package com.example.cse438.studio4.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.cse438.studio4.App
import com.example.cse438.studio4.R
import com.example.cse438.studio4.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_sign_in.*

@SuppressLint("ValidFragment")
class SignInFragment(context: Context): Fragment() {

    private var parentContext = context

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onStart() {
        super.onStart()

        sign_in.setOnClickListener {
            val email = email.text.toString()
            val password = password.text.toString()

            App.firebaseAuth = FirebaseAuth.getInstance()

            App.firebaseAuth!!.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.d(tag, "sign in with email success.")
                        this.activity?.finish()
                    }
                    else{
                        Log.e(tag, "sign in with email failed", task.exception)
                        val toast = Toast.makeText(this.parentContext, "Authentication failed.", Toast.LENGTH_SHORT)
                        toast.setGravity(Gravity.CENTER, 0, 0)
                        toast.show()
                    }
                }


            // TODO: Implement sign in with email and password; if user authenticates successfully, finish activity; else make a toast
        }
    }
}