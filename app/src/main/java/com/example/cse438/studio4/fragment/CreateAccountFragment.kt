package com.example.cse438.studio4.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.cse438.studio4.App
import com.example.cse438.studio4.R
import com.example.cse438.studio4.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_create_account.*

@SuppressLint("ValidFragment")
class CreateAccountFragment(context: Context) : Fragment() {

    private var parentContext = context

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_create_account, container, false)
    }

    override fun onStart() {
        super.onStart()

        create_account.setOnClickListener {
            val firstName = first_name.text.toString()
            val lastName = last_name.text.toString()
            val email = email.text.toString()
            val username = username.text.toString()
            val password = password.text.toString()

            if (firstName != "" && lastName != "" && email != "" && username != "" && password != "") {
                // TODO: Make a firebase call here to create a user with email and password

                Log.d(tag, "1")
                App.firebaseAuth = FirebaseAuth.getInstance()
                Log.d(tag, "2")
                App.firebaseAuth!!.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                    Log.d(tag, "3")
                    if (task.isSuccessful) {

                        Log.d(tag, "create user with email success.")
                        val userId = task.result?.user?.uid
                        val newUser = User(firstName, lastName, email, username)

                        val db = FirebaseFirestore.getInstance()
                        db.document("users/$userId").set(newUser).addOnCompleteListener {
                            if (it.isSuccessful) {
                                Log.d(tag, "add new user to database success.")
                                activity?.finish()
                            }
                            else {
                                Log.e(tag, "create user with email failed.", it.exception)
                            }
                        }
                        Toast.makeText(parentContext, "sign up success", Toast.LENGTH_SHORT).show()
                    } else {
                        Log.e(tag, "sign up with email failed", task.exception)
                        Toast.makeText(parentContext, "sign up failed.", Toast.LENGTH_SHORT).show()
                    }
                }
                // TODO: Store the user's data in collection "users" and document "<userId>" (DO NOT store password)
            } else {
                Toast.makeText(parentContext, "Must fill all fields", Toast.LENGTH_SHORT).show()
            }
        }
    }
}