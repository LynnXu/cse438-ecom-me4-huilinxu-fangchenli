package com.example.cse438.studio4.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.widget.Toast
import com.example.cse438.studio4.R
import com.example.cse438.studio4.adapter.ReviewAdapter
import com.example.cse438.studio4.model.Review
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_product_reviews.*

class ProductReviewsActivity : AppCompatActivity() {
    private var reviewList = ArrayList<Review>()
    private lateinit var adapter: ReviewAdapter

    private lateinit var itemId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_reviews)

        this.itemId = intent.getStringExtra("ItemId")


        adapter = ReviewAdapter(this, reviewList)
        review_items_list.adapter = adapter
    }

    override fun onStart() {
        super.onStart()

        val db = FirebaseFirestore.getInstance()

        db.document("items/$itemId").get().addOnCompleteListener {
            if (it.isSuccessful) {
                val map = it.result as MutableMap<String, Any>

                val rList = map["reviews"] as ArrayList<HashMap<String, Any>>

                if (rList.size != 0) {
                    rList.forEach { element ->
                        val newReview = Review(
                            element["body"] as String,
                            element["isAnonymous"] as Boolean,
                            element["userId"] as String,
                            element["username"] as String,
                            element["date"] as Timestamp
                        )
                        this.reviewList.add(newReview)
                    }
                    this.adapter.notifyDataSetChanged()
                }
            } else {
                Log.e("start", "Get item id failed.", it.exception)
                val toast = Toast.makeText(this, "Get item id failed.", Toast.LENGTH_SHORT)
                toast.setGravity(Gravity.CENTER, 0, 0)
                toast.show()
            }
        }
    }

    override fun onBackPressed() {
        this.finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                this.finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}